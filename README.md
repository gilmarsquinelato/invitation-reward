# invitation-reward

## Prerequisites

[Leiningen][]

[MongoDB][] 

[leiningen]: https://github.com/technomancy/leiningen
[mongodb]: https://www.mongodb.com/

## Running

### Start the web server running: 

```
lein ring server 
```

### To send invitation list:
 
1. Do a POST request to http://localhost:3000/invites (**text file to upload** or **text/plain body**)
2. Do a GET request to http://localhost:3000/invites

## Testing

To run the unit tests you just need to run the following command: 

```
lein test
```