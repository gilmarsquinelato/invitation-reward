(ns invitation-reward.models.invite-test
  (:require [clojure.test :refer :all]
            [monger.collection :as mc]
            [invitation-reward.database :as db]
            [invitation-reward.models.invite :as invite]
            [invitation-reward.helpers.database :refer :all]
            [invitation-reward.helpers.helper :refer :all]))

(use-fixtures :once setup-db)

(deftest test-database
  (testing "insert"
    (invite/insert {:code 1})
    (let [model (invite/get-one 1)]
      (is (not-nil? model))))

  (testing "update"
    (invite/update-record 1 {:code 2})
    (let [model (invite/get-one 2)]
      (is (not-nil? model))))

  (testing "update-set"
    (invite/update-set 2 {:code 1})
    (let [model (invite/get-one 1)]
      (is (not-nil? model))))

  (testing "update-inc"
    (invite/update-inc 1 {:code 1})
    (let [model (invite/get-one 2)]
      (is (not-nil? model))))

  (testing "insert-default"
    (invite/insert-default 4 1)
    (let [model (invite/get-one 4)]
      (is (not-nil? model))))

  (testing "get-rank"
    (mc/remove db/db invite/document)
    (invite/insert {:code 1 :points 1})
    (invite/insert {:code 2 :points 2})
    (let [rank (invite/get-rank)
          first (first rank)
          second (second rank)]
      (is (= (:points first) 2))
      (is (= (:points second) 1)))))
