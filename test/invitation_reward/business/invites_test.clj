(ns invitation-reward.business.invites_test
  (:require [clojure.test :refer :all]
            [monger.collection :as mc]
            [invitation-reward.database :as db]
            [invitation-reward.models.invite :as invite-db]
            [invitation-reward.business.invites :as invites]
            [invitation-reward.helpers.database :refer :all]
            [invitation-reward.helpers.helper :refer :all]))

(use-fixtures :once setup-db)

(deftest invites-test
  (testing "get-points"
    (is (= (#'invites/get-points 0) 1.0))
    (is (= (#'invites/get-points 1) 0.5))
    (is (= (#'invites/get-points 2) 0.25)))

  (testing "new-invite"
    (mc/remove db/db invite-db/document)
    (let [model (#'invites/new-invite 2 1)]
      (is (not-nil? model))
      (is (= (:code model) 2))
      (is (= (:inviter-code model) 1))))

  (testing "increase-points"
    (mc/remove db/db invite-db/document)
    (#'invites/new-invite 2 1)
    (#'invites/increase-points 2 1)
    (let [model (invite-db/get-one 2)]
      (is (= (:points model) 1.0))))

  (testing "get-inviter"
    (let [model (#'invites/get-inviter 10)]
      (is (not-nil? model))
      (is (= (:code model) 10))))

  (testing "set-already-invited-if-not"
    (let [model (#'invites/new-invite 11 10)]
      (is (not-nil? model))
      (is (= (:already-invited model) false))
      (#'invites/set-already-invited-if-not 11 false)
      (let [new-model (invite-db/get-one 11)]
        (is (= (:already-invited new-model) true)))))

  (testing "update-inviter-invites"
    (mc/remove db/db invite-db/document)
    (let [model (#'invites/new-invite 2 1)]
      (is (= (count (:inviteds model)) 0))
      (#'invites/update-inviter-invites model 3)
      (let [new-model (invite-db/get-one 2)
            inviteds (:inviteds new-model)]
        (is (= (count inviteds) 1))
        (is (= (first inviteds) 3)))))

  (testing "new-invite-if-not-exists"
    (#'invites/new-invite-if-not-exists 5 4)
    (let [model (invite-db/get-one 5)]
      (is (not-nil? model))
      (is (= (:code model) 5))
      (is (= (:inviter-code model) 4))))

  (testing "give-points-to-inviter"
    (mc/remove db/db invite-db/document)
    (#'invites/new-invite 1 -1)
    (#'invites/new-invite 2 1)
    (#'invites/new-invite 3 2)

    (#'invites/give-points-to-inviter 3 0)

    (let [model (invite-db/get-one 1)]
      (is (not-nil? model))
      (is (= (:points model) 0.25)))

    (let [model (invite-db/get-one 2)]
      (is (not-nil? model))
      (is (= (:points model) 0.5)))

    (let [model (invite-db/get-one 3)]
      (is (not-nil? model))
      (is (= (:points model) 1.0))))

  (testing "process-invite"
    (mc/remove db/db invite-db/document)
    (#'invites/process-invite 1 2)
    (#'invites/process-invite 1 3)
    (#'invites/process-invite 3 4)
    (#'invites/process-invite 2 4)
    (#'invites/process-invite 4 5)
    (#'invites/process-invite 4 6)

    (#'invites/process-invite 4 6) ;; Duplicate invites should not be processed

    (let [model (invite-db/get-one 1)]
      (is (not-nil? model))
      (is (= (:points model) 2.5)))

    (let [model (invite-db/get-one 3)]
      (is (not-nil? model))
      (is (= (:points model) 1.0)))

    (let [model (invite-db/get-one 2)]
      (is (not-nil? model))
      (is (= (:points model) 0.0)))

    (let [model (invite-db/get-one 4)]
      (is (not-nil? model))
      (is (= (:points model) 0.0)))

    (let [model (invite-db/get-one 5)]
      (is (not-nil? model))
      (is (= (:points model) 0.0)))

    (let [model (invite-db/get-one 6)]
      (is (not-nil? model))
      (is (= (:points model) 0.0))))

  (testing "parse-invite"
    (let [parsed-invite (#'invites/parse-invite "1 2")]
      (is (= (:inviter parsed-invite) 1))
      (is (= (:invited parsed-invite) 2))))

  (testing "parse-invites"
    (let [parsed-invites (#'invites/parse-invites "1 2\n2 3")
          first-invite (first parsed-invites)
          second-invite (second parsed-invites)]
      (is (= (:inviter first-invite) 1))
      (is (= (:invited first-invite) 2))

      (is (= (:inviter second-invite) 2))
      (is (= (:invited second-invite) 3))))

  (testing "process-invites"
    (mc/remove db/db invite-db/document)
    (#'invites/process-invites "1 2\n2 3")

    (let [n1 (invite-db/get-one 1)
          n2 (invite-db/get-one 2)
          n3 (invite-db/get-one 3)]
      (is (not-nil? n1))
      (is (not-nil? n2))
      (is (not-nil? n3))))

  (testing "invites"
    (mc/remove db/db invite-db/document)
    (let [response (invites/invites "1 2")]
      (is (= response {:status 200})))))