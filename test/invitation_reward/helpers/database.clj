(ns invitation-reward.helpers.database
  (:require [monger.core :as mg]
            [invitation-reward.database :as db]))

(defn setup-db [f]
  (db/connect "test")
  (f)
  (mg/drop-db db/conn "test"))