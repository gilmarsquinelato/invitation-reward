(ns invitation-reward.database-test
  (:require [clojure.test :refer :all]
            [invitation-reward.database :as db]
            [invitation-reward.helpers.database :refer :all]))

(use-fixtures :once setup-db)

(def document "test")

(deftest test-database
  (testing "connecting to database"
    (is (not= db/db nil)))

  (testing "insert"
    (db/insert document {:valid true})
    (let [model (db/get-one document {:valid true})]
      (is (not= model nil))))

  (testing "update"
    (db/update-record document {:valid true} {:valid false})
    (let [model (db/get-one document {:valid false})]
      (is (not= model nil)))))
