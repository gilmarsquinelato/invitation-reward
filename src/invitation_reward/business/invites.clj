(ns invitation-reward.business.invites
  (:require [clojure.string :as str]
            [invitation-reward.models.invite :as invite-db]
            [invitation-reward.helpers.helper :refer :all]))

;; PROCESS INPUT
(defn- get-points [level]
  (Math/pow 1/2 level))

(defn- new-invite [invite-code inviter-code]
  (invite-db/insert-default invite-code inviter-code)
  (invite-db/get-one invite-code))

(defn- increase-points [code points]
  (invite-db/update-inc code {:points points}))

(defn- get-inviter [code]
  (let [model (invite-db/get-one code)
        return (if (nil? model) (new-invite code -1) model)]
    return))

(defn- set-already-invited-if-not [code already-invited]
  (if (not already-invited)
    (invite-db/update-set code {:already-invited true})))

(defn- update-inviter-invites [model invited-code]
  (invite-db/update-set (model :code)
                        {:inviteds (->> invited-code
                                        (conj (model :inviteds))
                                        (distinct))}))

(defn- new-invite-if-not-exists [invited-code inviter-code]
  (if (nil? (invite-db/get-one invited-code))
    (new-invite invited-code inviter-code)))

(defn- give-points-to-inviter [code level]
  (let [model (invite-db/get-one code)]
    (if (not-nil? model)
      (do
        (increase-points code (get-points level))
        (recur (model :inviter-code) (inc level))))))

(defn- process-invite [inviter-code invited-code]
  (let [inviter-model (get-inviter inviter-code)
        inviteds (inviter-model :inviteds)
        parent-inviter-code (inviter-model :inviter-code)
        invited-first-time-by (not (some #{invited-code} inviteds))
        already-invited (inviter-model :already-invited)]

    (new-invite-if-not-exists invited-code inviter-code)

    (if (and invited-first-time-by
             (not already-invited))
        (give-points-to-inviter parent-inviter-code 0))

    (set-already-invited-if-not inviter-code already-invited)
    (update-inviter-invites inviter-model invited-code)))

(defn- parse-invite [invite]
  (let [splitted (str/split invite #" ")
        inviter-code (read-string (first splitted))
        invited-code (read-string (second splitted))]
    {:inviter inviter-code :invited invited-code}))

(defn- parse-invites [invites]
  (->> (str/split-lines invites)
       (map parse-invite)))

(defn- process-invites [invites]
  (doseq [invite (parse-invites invites)]
    (process-invite (invite :inviter) (invite :invited))))

(defn invites [invites-str]
  (try
    (process-invites invites-str)
    {:status 200}
    (catch Exception e {:status 500 :body (.getMessage e)})))

;; GET RANK
(defn- rank-fields [rank]
  {:code (rank :code) :points (rank :points)})

(defn get-rank []
  (try
    (->> (invite-db/get-rank)
         (map rank-fields))
    (catch Exception e {:status 500 :body (.getMessage e)})))