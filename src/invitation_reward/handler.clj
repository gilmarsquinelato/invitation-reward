(ns invitation-reward.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [ring.util.response :refer [response]]
            [invitation-reward.routes.invites :as invites]
            [invitation-reward.database :as db]))

(db/connect)

(defroutes app-routes
  (context "/invites" [] invites/invites-routes)
  (route/not-found "Not Found"))

(def app
  (-> app-routes
      (wrap-json-response)
      (wrap-defaults api-defaults)))