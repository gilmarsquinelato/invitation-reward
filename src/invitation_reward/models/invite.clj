(ns invitation-reward.models.invite
  (:refer-clojure :exclude [sort find])
  (:require [monger.query :refer :all]
            [monger.operators :refer :all]
            [invitation-reward.database :as db]))


(def document "invite")

(defn get-rank []
  (with-collection db/db document
    (find {})
    (fields [:code :points])
    (sort (array-map :points -1 :code 1))))

(defn get-one [code]
  (db/get-one document {:code code}))

(defn insert [parameters]
  (db/insert document parameters))

(defn update-record [code parameters]
  (db/update-record document {:code code} parameters))

(defn update-set [code parameters]
  (update-record code {$set parameters}))

(defn update-inc [code parameters]
  (update-record code {$inc parameters}))

(defn insert-default [invite-code inviter-code]
  (insert {:code invite-code
           :inviter-code inviter-code
           :points 0.0
           :already-invited false
           :inviteds []}))
