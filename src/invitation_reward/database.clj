(ns invitation-reward.database
  (:require [monger.core :as mg]
            [monger.collection :as mc]
            [monger.json]))

(def db nil)
(def conn nil)

(defn connect
  ([] (connect "invitation-reward"))
  ([database]
    (let [connection (mg/connect)
          database-obj (mg/get-db connection database)]
      (alter-var-root #'conn (constantly connection)
      (alter-var-root #'db (constantly database-obj))))))

(defn get-one [document parameters]
  (mc/find-one-as-map db document parameters))

(defn insert [document parameters]
  (mc/insert db document parameters))

(defn update-record [document query parameters]
  (mc/update db document query parameters))