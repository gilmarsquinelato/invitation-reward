(ns invitation-reward.routes.invites
  (:require [compojure.core :refer :all]
            [ring.util.response :refer [response]]
            [invitation-reward.business.invites :as invites]))

(defn get-invites []
  (response (invites/get-rank)))

(defn post-invites [body]
  (->> body
       (slurp)
       (invites/invites)))

(defroutes invites-routes
  (GET "/" [] (get-invites))
  (POST "/" {body :body} (post-invites body)))